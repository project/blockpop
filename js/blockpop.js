/*
 * Pop that popup up on the page.
 */
(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.blockPopPopup = {
    "attach": function(context, settings) {
      var $popup = $('#block-blockpop-popup', context);
      if (!!settings.blockpop) {
        var dbStamp = settings.blockpop.timestamp;
        if ($popup.length && this.shouldShowPopup(dbStamp, settings.blockpop.cookieExpiration,  settings.blockpop.startDateUtc, settings.blockpop.endDateUtc)) {
          setTimeout(Drupal.behaviors.blockPopPopup.initAndTriggerPopup, settings.blockpop.timeDelayInSeconds * 1000, $popup, settings);
        }
      }
    },
    initAndTriggerPopup: function($popup, settings) {
      if (!$popup.hasClass('blockpop-popup-processed')) {
        $popup.addClass('blockpop-popup-processed').addClass('modal')
          .css('border-radius', settings.blockpop.borderRadius + 'px')
          .css('padding', settings.blockpop.padding + 'px');
        Drupal.behaviors.blockPopPopup.resizePopup($popup);
        if (!!settings.blockpop.popupStyle) {
          $popup.addClass(settings.blockpop.popupStyle);
        }
        var modalOptions = {
          "zIndex": parseInt(settings.blockpop.zIndex) - 1
        };
        if (settings.blockpop.timeDelayInSeconds > 0) {
          modalOptions['fadeDuration'] = 1000;
          modalOptions['fadeDelay'] = 0.5;
        }
        $popup.modal(modalOptions);
        $popup.find('.content a').click(function(event) {
          $('#block-blockpop-popup .content a').trigger('blockpop:click', [event]);
          if (!!window._gaq || !!window.ga) {
            var $this = $(this);
            var gaEventCategory = settings.blockpop.gaEventCategory ? settings.blockpop.gaEventCategory : null;
            var gaEventAction = settings.blockpop.gaEventAction ? settings.blockpop.gaEventAction : null;
            var href = $this.attr('href');
            if (!!href) {
              if ($this.data('gaEventCategory') && $this.data('gaEventAction')) {
                gaEventCategory = $this.data('gaEventCategory');
                gaEventAction = $this.data('gaEventAction');
              }
              // Classic Analytics ga.js
              if (!!window._gaq && gaEventCategory && gaEventAction) {
                event.preventDefault();
                _gaq.push(['_set', 'hitCallback', function() {
                    document.location = href;
                }]);
                _gaq.push(['_trackEvent', gaEventCategory, gaEventAction]);
                return false;
              }
              // Universal Analytics analytics.js
              if (!!window.ga && gaEventCategory && gaEventAction) {
                event.preventDefault();
                ga('send', 'event', gaEventCategory, gaEventAction, {
                  "useBeacon": true,
                  "hitCallback": function(){
                    document.location = href;
                  }
                });
                return false;
              }
            }
          }
        });

        var expirationHours = parseInt(settings.blockpop.cookieExpirationHours);
        var now = new Date();
        var expirationDate = new Date(now.getTime() + (expirationHours * 60 * 60 * 1000));
        var dbStamp = settings.blockpop.timestamp;
        $.cookie('blockpop_popup_last_shown', dbStamp, {
          expires: expirationDate
        });
        $popup.find('img').load(function(){
          Drupal.behaviors.blockPopPopup.resizePopup($popup);
        });
        $(window).load(function(){
          Drupal.behaviors.blockPopPopup.resizePopup($popup);
        }).resize(function(){
          Drupal.behaviors.blockPopPopup.resizePopup($popup);
        });
      }
    },
    resizePopup: function($popup) {
      //document.body.clientWidth is width without scrollbar
      if (document.body.clientWidth < Drupal.settings.blockpop.width) {
        $popup.width(document.body.clientWidth - 20 - parseInt(Drupal.settings.blockpop.padding));
      }
      else {
        $popup.width(parseInt(Drupal.settings.blockpop.width) - parseInt(Drupal.settings.blockpop.padding));
      }
      $.modal.resize();
    },
    shouldShowPopup: function(dbStamp, cookieExpiration, startDateUtc, endDateUtc) {
      var lastShown = $.cookie('blockpop_popup_last_shown');
      var show = true;
      var now = new Date().getTime();
      now = Math.round(now / 1000);
      var start = new Date();
      start.setTime(startDateUtc * 1000);
      if (startDateUtc != null && now < startDateUtc) {
        show = false;
      }
      if (endDateUtc != null && now > endDateUtc ) {
        show = false;
      }
      if (lastShown == dbStamp && cookieExpiration != 'always') {
        show = false;
      }
      return show;
    }
  };
})(jQuery, Drupal, this, this.document);
