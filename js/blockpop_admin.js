/*
 * @file
 * JS for admin interface
 */


(function ($, Drupal, window, document, undefined) {

  Drupal.behaviors.blockpopAdminEnableDatefields = {
    "attach": function(context, settings) {
      $('.blockpop-datetimefield-enable', context)
        .once()
        .bind('change', function(){
          var dateFieldName = $(this).data('blockpopdatetimeenable');
          var $dateTimeField = $('input[name="'+ dateFieldName +'"], select[name="'+ dateFieldName +'_tz"]');
          if ($(this).is(':checked')) {
            $dateTimeField.removeAttr('disabled').parent().css('display', 'block');
          }
          else {
            $dateTimeField.attr('disabled', 'disabled').parent().css('display', 'none');
          }
        }).trigger('change');
    }
  };

  Drupal.behaviors.blockpopAdminDatepickers = {
    "attach": function(context, settings) {
      $('.blockpop-datetime-field', context)
        .once()
        .datetimepicker({
          controlType: 'select',
          timeFormat: 'hh:mm tt',
       });
    }
  };

  Drupal.behaviors.blockpopShowCookieLength = {
    "attach": function(context, settings) {
      var $timePeriod = $('#blockpop-time-period-wrapper');
      $('.blockpop-cookie-expiration input[type="radio"]').once().each(function(){
        $(this).bind('change', function(){
          if ($(this).val() == 'length') {
            $timePeriod.show();
          }
          else {
            $timePeriod.hide();
          }
        });
      });
      $('.blockpop-cookie-expiration input[type="radio"]:checked').trigger('change');
    }
  };



})(jQuery, Drupal, this, this.document);
