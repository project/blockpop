## INTRODUCTION

The BlockPop module provides a highly configurable block that will show a popup 
when a page containing the block is viewed. 

## Installation

BlockPop requires the Drupal Libraries module as well as two javascript libraries.

The Libraries module can be found at http://drupal.org/project/libraries.

The jquery-modal javascript library can be found at 
https://github.com/kylefox/jquery-modal and should be placed in 
sites/all/libraries/jquery-modal. Please use version 0.5.x! Currently it doesn't work
with the 0.6.x release.

The jQuery UI Timepicker AddOn library can be found at
 https://github.com/trentrichardson/jQuery-Timepicker-Addon
It should be placed in sites/ll/libraries/jquery-timepicker-addon. As of this 
writing the latest version is 1.6.1 and the timepicker works.

## Configuration

After enabling the module and installing the required libraries, make sure to set
up the block at:
http://[yoursite]/admin/structure/block/manage/blockpop/popup/configure
In particular, make sure to take a look at popup start and end dates, popup
frequency, and set the block to show on the proper pages.

